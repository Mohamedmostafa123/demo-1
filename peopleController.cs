﻿using APIDEMO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace APIDEMO.Controllers
{
    public class peopleController : ApiController
    {
        List<person> people = new List <person>();
        public peopleController()
        {
            people.Add(new person { firstname = "sawsan", lastname = "aLi", id = 1 });
            people.Add(new person { firstname = "ahmed", lastname = "maged", id = 2 });
            people.Add(new person { firstname = "bahaa", lastname = "Mohamed", id = 3 });
            people.Add(new person { firstname = "shady", lastname = "ALi", id = 4});

        }

        [Route("api/people/Getfirstnames")]
        [HttpGet]

        public List<string> Getfirstnames()
        {
         
           List<string> output = new List<string> ();
            foreach (var p in people)
            {
                output.Add(p.firstname);
            }
            return output;
        }

        // GET: api/people
        public List <person> Get()
        {
            return people;
        }

        // GET: api/people/5
        public person  Get(int id)
        {
            return people.Where(x => x.id == id).FirstOrDefault();
        }

        // POST: api/people
        public void Post(person val)
        {
            people.Add(val);
        }

        // PUT: api/people/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/people/5
        public void Delete(int id)
        {
        }
    }
}
